#!/usr/bin/python3
import numpy as np

# Utility function to check if a number's placement is possible
# Params
#	y:     Vertical position
#	x:     Horiztonal position
#	n:     Number to be checked
#	board: The board we're solving
# Return: True if n can be placed, false otherwise

def possible(y, x, n, board):
	for i in range(0, 9):
		if board[y][i] == n:
			return False
	for i in range(0, 9):
		if board[i][x] == n:
			return False
	x0 = (x//3)*3
	y0 = (y//3)*3
	for i in range(0, 3):
		for j in range(0, 3):
			if board[y0+i][x0+j] == n:
				return False
	return True
	

# Recursive function that solves the board if possible. Prints solved board if possible, unsolved if not
# Params
#	board: The board that we'll be attempting to solve
# Return: None. Prints solved board
def solve(board):
	# Iterate through each position in each row
	for y in range(9):
		for x in range(9):
			# If current position is empty, iterate through 1-9 and check if each num can be placed there
			if board[y][x] == 0:
				for n in range(1, 10):
					# If current number can be placed, place it and then recursively call solve. If the entire board can't be solved w/ that placement, backtrack
					if possible(y, x, n, board):
						board[y][x] = n
						solve(board)
						board[y][x] = 0
				return

	# Print board with numpy's matrix() function
	print(np.matrix(board))
	input('Hit enter for another solution. Hit Ctrl-C to exit')
