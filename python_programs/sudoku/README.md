Sudoku puzzle solver. This is a brute force solver, as it doesn't use any particular strategy; instead, it uses recursion and backtracking to attempt a solution.
I'm yet to find any cases where this solver fails to work. If you see any issues, please make note of it.
This isn't the most elegant or pretty solution. However, since sudoku has a consistent board (81 spaces every time), I figured an ugly/inefficient solution would work perfectly fine for this case.
This particular solver is based on a Computerphile video found at the following link: [Computerphile Video]

[Computerphile Video]: https://www.youtube.com/watch?v=G_UYXzGuqvMef
