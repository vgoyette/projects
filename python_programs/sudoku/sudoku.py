#!/usr/bin/python3
import pprint
import sys
import numpy as np
import sudokusolver as sudsolve

# Initialize board as empty list. This list will contain lists (the rows of the board)
board = []

print('Enter each row on a single line, with each number in the row separated by a space. If the given space is empty, enter a 0.')

# Get board as input
for i in range(9):
	# Split numbers by spaces and cast to int
	x = list(map(int, input().split()))

	# If there aren't nine numbers, input is invalid; get it again
	while len(x) != 9:
		print('Invalid number of numbers in that row. Try Again')
		x = list(map(int, input().split()))
	
	# Add list of numbers (row) to the board
	board.append(x)

# Solve the board
sudsolve.solve(board)
