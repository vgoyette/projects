#!/usr/bin/python3

import sys

progname = sys.argv[0]

def usage(status=1):
	print(f'''{progname} min max 
(min > 1 and max <= 1,000,000)''')
	sys.exit(status)


if len(sys.argv[1:]) != 2:
	usage(1)

try:
	minNum = int(sys.argv[1])
	maxNum = int(sys.argv[2])
	if minNum > maxNum:
		print('Min cannot be greater than max. Exiting...')
		sys.exit(1)
except ValueError:
	print('Min and max must be integer arguments. Exiting...')
	sys.exit(1)

maxCycleLen = 0
maxCycleNum = 0
for num in range(minNum, maxNum+1):
	currCycleLen = 1
	currCycleNum = num
	while num != 1:
		if num % 2 == 1:
			num = (3 * num) + 1
		else:
			num = num / 2
		currCycleLen += 1
	
	if currCycleLen > maxCycleLen:
		maxCycleLen = currCycleLen
		maxCycleNum = currCycleNum

print(minNum, maxNum, maxCycleNum, maxCycleLen) 
