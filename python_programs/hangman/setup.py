#!/usr/bin/python3

def printBoard(limit):
	if limit == 6:
		print(
			'''
                |------------
                |           |
                |           |
                |
                |
                |
                |
                |
         ----------------

         You have 6 guesses left
			'''
		)
	elif limit == 5:
		print(
			'''
                |------------
                |           |
                |           |
                |           O
                |
                |
                |
                |
         ----------------
		 
         You have 5 guesses left
		 	'''
		)
	elif limit == 4:
		print(
			'''
                |------------
                |           |
                |           |
                |           O
                |           |
                |           |
                |
                |
         ----------------

         You have 4 guesses left
		 	'''
		)
	elif limit == 3:
		print(
			'''
                |------------
                |           |
                |           |
                |           O
                |           |/
                |           |
                |
                |
         ----------------

         You have 3 guesses left
		 	'''
		)		
	elif limit == 2:
		print(
			'''
                |------------
                |           |
                |           |
                |           O
                |          \\|/
                |           |
                |
                |
         ----------------

         You have 2 guesses left
		 	'''
		)
	elif limit == 1:
		print(
			'''
                |------------
                |           |
                |           |
                |           O
                |          \\|/
                |           |
                |          /
                |
         ----------------

         You have 1 guess left
		 	'''
		)
	elif limit == 0:
		print(
			'''
                |------------
                |           |
                |           |
                |           O
                |          \\|/
                |           |
                |          / \\
                |
         ----------------

         You are out of guesses
		 	'''
		)