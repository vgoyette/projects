#!/usr/bin/python3

import sys
import random_word
from setup import printBoard

#======================FUNCTIONS===========================
def usage(exStatus=0):
	print(f'Usage: {sys.argv[0]} minLetters maxLetters')
	sys.exit(exStatus)

def checkLetter(letter, word, printList):
	num = 0
	i = 0
	print('     ', end='')
	for char in word:
		if letter == char:
			printList[i] = letter
			print(printList[i], end='')
			num = num + 1
		else:
			print(printList[i], end='')
		i = i + 1
	print('\n')
	return num


#=========================MAIN=============================
def main():
	# Parse Command Line Arguments
	args = sys.argv[1:]

	if len(args) != 2 or args[0] == '-h':
		usage(1)

	while len(args):
		min = args.pop(0)
		max = args.pop(0)

	if min > max:
		usage(1)


	# Generate Random Word
	r = random_word.RandomWords()

	word = r.get_random_word(minLength=min, maxLength=max)

	# Set Up For Game-------------------------------------------
	correctLetters = 0
	incorrectGuesses = 0
	limit = 6

	printList = []
	i = 0
	while i < len(word):
		printList.append('_')
		i = i + 1
	
	print(f'     Word Length: {len(word)}')

	# Play Game--------------------------------------------------
	while limit > 0:
		letter = input('     Guess a letter: ')
		correct = checkLetter(letter, word, printList)
		if correct == 0:
			limit = limit - 1
		else:
			correctLetters = correctLetters + correct
		printBoard(limit)
	
		if correctLetters == len(word):
			print('     You win!')
			break
	if limit == 0:
		print(f'     You lose! The correct word was: {word}')

if __name__ == '__main__':
	main()
