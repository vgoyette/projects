#!/usr/bin/python3

import sys

args = sys.argv

if len(args) != 2:
	print(f'Usage: {sys.argv[0]} [number]')
	sys.exit(1)

try:
	num = int(sys.argv[1])
except:
	print('Argument must be an integer')
	sys.exit(1)

for i in range(num + 1):
	if i % 15 == 0:
		print(f'{i} fizzbuzz')
	elif i % 3 == 0:
		print(f'{i} fizz')
	elif i % 5 == 0:
		print(f'{i} buzz')
