#!/usr/bin/python3

# Program to calculate a student's semester GPA given each class's credit hours and letter grade
# This calculation was taken from the University of Notre Dame's calculation of GPA
# The ND method for calculating GPA can be found at https://registrar.nd.edu/students/gradefinal.php

import sys

#=====================================================================================
# Valid Letter Grades
gradesDict = {
	'A' : 4.00,
	'A-' : 3.67,
	'B+' : 3.33,
	'B' : 3.00,
	'B-' : 2.67,
	'C+' : 2.33,
	'C' : 2.00,
	'C-' : 1.67,
	'D+' : 1.33,
	'D' : 1.00,
	'D-' : 0.67,
	'F' : 0.00
	}

#======================================================================================
# Set up for input

points = 0
hours = 0
try:
	numClasses = int(input('Enter how many classes you took this semester: '))
except ValueError:
	print('Number of classes must be a number. Exiting...')
	sys.exit(1)

print()

classNames = []
classGrades = []
classCredits = []

#======================================================================================
# Get input and calculate GPA

i = 0
while i < numClasses:
	print(f'Class #{i+1}')

	#----> Get name of class
	name = input('Enter the name of the class: ')
	classNames.append(name)

	#----> Get number of credits for the class, add to total hours taken
	try:
		creds = int(input('Enter number of credits for course: '))
		while creds < 0:
			creds = int(input('Number of credits must be 0 or more. Enter again: '))
		classCredits.append(creds)
		hours = hours + creds
	except ValueError:
		print('Number of credits must be a number. Exiting...')
		sys.exit(1)

	#----> Get grade for the class
	grade = input('Enter letter grade for course (A-F): ')
	grade = grade.upper()
	while grade not in gradesDict:
		grade = input('Grade must be between A and F. Please enter again: ')
		grade = grade.upper()
	classGrades.append(grade)
	
	#----> Calculate GPA and quality points for that class, add to quality points total
	classGpa = gradesDict[grade]
	qual = classGpa*creds
	points = points + qual
	i+=1
	print()


#=======================================================================================
# Print out schedule with total GPA

print(f'                  Class Name         Credit Hours         Grade')
print('---------------------------------------------------------------')

for (name, credits, grade) in zip(classNames, classCredits, classGrades):
	print(f'{name:>28} {credits:>20} {grade:>13}')



gpa = points/hours
print('---------------------------------------------------------------')
print(f'{hours:>49}     GPA: {gpa:.2f}')
