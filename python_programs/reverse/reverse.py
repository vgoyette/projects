#!/usr/bin/python3

word = input('Input the string you\'d like to reverse: ')

revWord = ''

for char in reversed(word):
	revWord = revWord + char

print(revWord)
