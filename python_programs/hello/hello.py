#!/usr/bin/python3

import sys

prog_name = sys.argv[0]

def usage(exit_status = 0):
	print(f'Usage: {prog_name} name')
	sys.exit(exit_status)


def main():
	
	args = sys.argv[1:]
	if len(args) != 1:
		usage(1)
	
	name = args[0]

	if name == '-h':
		usage(0)

	print(f'Hello, {name}!')

main()

