#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char* argv[])
{
	if(argc != 2)
	{
		printf("Usage: ./%s [word]", argv[1]);
		return -1;
	}

	char* revWord = malloc(strlen(argv[1]));

	int j = 0;

	for(int i = strlen(argv[1]) - 1; i >= 0; i--)
	{
		revWord[j] = argv[1][i];
		j++;
	}

	printf("Reversed Word: %s\n", revWord);
	free(revWord);

	return 0;
	
}
