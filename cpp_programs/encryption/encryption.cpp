#include <iostream>
#include <unordered_map>
#include <string>
#include <fstream>
#include <string>
using namespace std;

int main(int argc, char* argv[])
{
	
	ifstream iFS;

	iFS.open("key.txt");

	if(!iFS)
	{
		cerr << "Unable to open key file\n";
		return 1;
	}

	unordered_map<char, char> decrypt;

	char key;
	char val;

	while(iFS >> key >> val) 							// Fill map with key/value pairs
		decrypt.insert({key, val});

	iFS.close();

	ifstream inFile;
	inFile.open("encryptedtext.txt");
	if(!inFile)
	{
		cerr << "Unable to open encrypted file\n";
		return 1;
	}

	ofstream outFile;
	outFile.open("decryptedtext.txt");

	if(!outFile)
	{
		cerr << "Unable to open decryptedtext.txt\n";
		return 1;
	}

	char curr;
	while(inFile >> curr)
	{

		if((curr >= 'a' && curr <= 'z') || (curr >= 'A' && curr <= 'Z'))
		{
			outFile << decrypt[curr];
		}
		else
	 		outFile << curr;
	}

	outFile.close();
	return 0;

}

