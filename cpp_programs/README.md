Program that decrypts a simple substitution cypher and outputs to a file. An example cypher and text file for testing purposes are included in key.txt and encryptedtext.txt, respectively.
When run with these files, output is alphabet in reverse ('zyxwvutsrqponmlkjihgfedcba')
