#include <iostream>
#include <string>
#include <cstdlib>

std::string progname;

int usage(int exit_status = 0)
{
	std::cout << "Usage: " << progname << " name\n";
	exit(exit_status);
}

int main(int argc, char* argv[])
{
	progname = argv[0];
	if(argc != 2)
	{
		usage(1);
	}

	std::string arg = argv[1];

	if(arg == "-h")
	{
		usage(0);
	}

	std::cout << "Hello, " << argv[1] << "!\n";

	return 0;
}
