#include <iostream>
#include <string>

using namespace std;

int main(int argc, char* argv[])
{
	if(argc != 2)
	{
		cout << "Usage: ./" << argv[0] << " [word]\n";
		return -1;
	}

	string word = argv[1];
	string revWord;

	for(int i = word.size()-1; i >= 0; i--)
		revWord+=word[i];

	cout << revWord << endl;

	return 0;

}
